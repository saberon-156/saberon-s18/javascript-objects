function pokemon(name, lvl, hp, dmg) {
	this.name = name;
	this.level = lvl;
	this.health = hp;
	this.damage = dmg;

	// methods

	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name)
	};

	this.hpStatus = function(target) {
	this.health = this.health - target.damage
		console.log(this.name + ' health dropped down to ' + this.health)
	};

	this.faint = function(){
		if(this.health<10) {
			console.log(this.name + ' fainted :(')
		} else if (this.health <= 12) {
			console.log(this.name + ' is low in health! Attack now!' )
		} else {
			console.log(this.name + ' has not yet fainted.')
		}
	}

}

// creating instance
let lugia = new pokemon('Lugia', 3, 30, 5);
let charizard = new pokemon('Charizard', 2, 25, 8);


lugia.tackle(charizard);
charizard.hpStatus(lugia);

charizard.tackle(lugia);
lugia.hpStatus(charizard);

lugia.tackle(charizard);
charizard.hpStatus(lugia);

charizard.tackle(lugia);
lugia.hpStatus(charizard);

lugia.tackle(charizard);
charizard.hpStatus(lugia);
charizard.faint()

charizard.tackle(lugia);
lugia.hpStatus(charizard);
lugia.faint()





